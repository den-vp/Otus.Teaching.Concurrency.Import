﻿using System;
using System.Diagnostics;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _generatorPath = @"../../../../Otus.Teaching.Concurrency.Import.DataGenerator.App/bin/Debug/net5.0/Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory ?? string.Empty, "customers.xml");
        private static string _generatorMode = "method";
        private static int _threadsCount = Environment.ProcessorCount;
        private static string _threadMode = "s";
        private static int _dataCount = 100_000;
        static void Main(string[] args)
        {
            if (args != null)
            {
                if (args.Length >= 1)
                {
                    _generatorMode = args[0];
                }

                if (args.Length >= 2)
                {
                    if (!int.TryParse(args[1], out _dataCount))
                        throw new InvalidDataException($"Аргумент №2 должен быть целочисленным");
                }

                if (args.Length >= 3)
                {
                    if (!int.TryParse(args[2], out _threadsCount))
                        throw new InvalidDataException($"Аргумент №3 должен быть целочисленным");
                }

                if (args.Length >= 4)
                {
                    _threadMode = args[3];
                }

                if (args.Length >= 5)
                {
                    _dataFilePath = args[4];
                }
            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            GenerateCustomersDataFile();
            new AppDbContext().Database.Migrate();
            new CustomerRepository().Reset();
            LoadData();
        }
        static void LoadData()
        {
            var customerList = new List<Customer>();
            var xmlParser = new XmlParser();
            customerList = xmlParser.Parse(_dataFilePath);

            switch (_threadMode)
            {
                case "p":
                    var tpLoader = new DataLoaderThreadPool(customerList, _threadsCount);
                    tpLoader.LoadData();
                    break;
                case "t":
                    var tLoader = new DataLoaderThreads(customerList, _threadsCount);
                    tLoader.LoadData();
                    break;
                default:
                    var sLoader = new SingleDataLoader(customerList);
                    sLoader.LoadData();
                    break;
            }
        }

        static void GenerateCustomersDataFile()
        {

            if (_generatorMode.Equals("method"))
            {
                Console.WriteLine("Генерация через метод...");

                var xmlGenerator = new XmlGenerator(_dataFilePath, _dataCount);
                xmlGenerator.Generate();
            }
            else
            {
                Console.WriteLine("Генерация через приложение...");
                var process = new Process();
                process.StartInfo.FileName = _generatorPath;
                process.StartInfo.Arguments = _dataFilePath + " " + _dataCount;
                process.Start();
                process?.WaitForExit();
            }
        }

    }
}