﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
            : IDataParser<List<Customer>>
    {
        public List<Customer> Parse(string path)
        {
            //Parse data
            Console.WriteLine("Parse");
            var customerList = new List<Customer>();
            XmlSerializer formatter = new XmlSerializer(typeof(CustomersList));

            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var customers = (CustomersList)formatter.Deserialize(fs);
                customerList = customers.Customers;
            }

            return customerList;
        }
    }

}